# Using ncurses


## Initialization

After inclusion of **_curses.h_** there must be the following code to initialize ncurses:

```cpp
	(void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
	/* finish is a function that ends curses and program */

	(void) initscr();      /* initialize the curses library */
	keypad(stdscr, TRUE);  /* enable keyboard mapping */
	(void) nonl();         /* tell curses not to do NL->CR/NL on output */
	(void) cbreak();       /* take input chars one at a time, no wait for \n */
	(void) noecho();       /* don't echo input */

	/* Code to write something to screen */

	refresh();						/* Draws screen */
```

The **_cbreak_** allows to use terminal instructions (like CTRL+C, CTRL+Z) and keep process to the terminal. Instead of this,
if **_raw()_** is used, each character is passed directly to the program, wich allow the program to process everything.

The **_nl()_** and **_nonl()_** disables the tranlation of return key into new line on input, and newline into return and line feed in output.
this allows to enable better use of line-feed capability, resulting in faster cursor motion. It algo enable curses to detect return key. [[1]]

The **_refresh_** function draws the contents of the base screen, but has no influence on the windows. For windows there is a specific
function [1](#Windows)

If program should not show the cursor in the window, then run:
```cpp
	(void) curs_set(0) /* 0: invisible; 1: normal; 2: very visible; */
```

To find the current terminal size it should be used the function:

```cpp
	(void) getmaxyx(windox, y, x);
	 /* win: window pointer (use stdscr to base terminal window)
		*   y, x: y, x co-ordinates will be put into this variables (variables passed by reference, not by pointer) 
	  */
```

Program should always terminate ncurses with function:
```cpp
	(void) endwin(); /* End curses mode		  */
```


## Windows

To create a new window we use the function

```cpp
	(WINDOW *) newwin(height, width, starty, startx); 
	box(window, yChar, xChar);
	/* 0, 0 gives default characters 
	 * for the vertical and horizontal
	 * lines			*/
	wrefresh(window);		/* Show that box		*/
```


## Cursor Movement

It's possible to move the cursor around the screen. For that exists two function:

```cpp
	/* Moves the cursor to the supplied coordinates. */
	/* Returns OK or ERROR */
	(int) move (y, x);
	/* Same as before but in a window */
	(int) wmove (window, y, x);
```


## Output

### Character output

The functions below can be used to output a character to the screen. After insertion, they move the cursor to the next position.

```cpp
	/* Prints the "char" to current cursor position with the attributes or'd (optional) */
	addch( char | A_BOLD | A_UNDERLINE); 
	waddch(window, char | A_BOLD | A_UNDERLINE); /* same as previous but for a window) */

	/* These function add cursor movement previous to character insertion */
	mvaddch(y, x, char);
	mvwaddch(window, y, x, char);
```


### String (Char *) output

The functions below print an array of char to the screen. If using strings, it must be called on the string the function _**.c_str()**_.
These function act like printf, so you can print any value with wildcards.

```cpp
	/* Prints the "string" to current cursor position */
	char * strPointer;
	std::string str;
	int number;

	printw( strPointer ); 
	printw( str.c_str());
	printw( "%d", number);
	wprintw(window, str.c_str()); /* same as previous but for a window) */

	/* These function add cursor movement previous to string insertion */
	mvprintw(y, x, str.c_str());
	mvwprintw(window, y, x, str.c_str());
```


## Input

### Character input

The function _**getch**_ waits for a user input on base screen. The equivalent _**wgetch**_ does the same but on a window.

```cpp
	/* Blocks the program waiting for a user input (char by char) on the base screen*/
	(char) getch();

	/* Blocks the program waiting for a user input (char by char) on a window */
	(char) wgetch(window);
```


### String (Char *) input

The functions below read every character inserter by user until a new line sent. 
The functions _**scanw**_ and _**wscanw**_ add the functionality of scanf;

```cpp
	/* Reads array of char from terminal until new line */ 
	/* Array is passed by reference, not by pointer */
	/* Return value is OK or ERR */
	(int) getstr( char * );
	(int) wgetstr( window, char * ); /* same as previous but on window */
	/* The equivalent function with cursor movement previous to reading */
	(int) mvgetstr(y, x, char * );
	(int) mvwgetstr(window, y, x, char * );

	/* Same as getstr but with string convertion of reading to the var_list variables */
	(int) scanw( char *, var_list);
	(int) wscanw(window, char *, var_list));
	/* The equivalent function with cursor movement previous to reading */
	(int) mvscanw(y, x, char *, var_list);
	(int) mvwscanw(window, y, x, char *, var_list);
```


## Keypad Keys

To read keyboard keys like UP, DOWN, etc, it must be enabled 
through the function:

```cpp
	keypad(window, TRUE); // To enable reading keyboard this must be run;

```

Then there are macros to all keys:

* KEY_UP
* KEY_DOWN

## Attributes and Color

Attributes are certain configurations that change the current way
curses display text. To use attributes we must turn them on, and turn them off when we're done with them.
To use them:

```cpp
	wattron(window, attr); // turn attr on to given window
	wattroff(window, attr);// turn attr off to given window
```

### Invert colors

The attribute _**A_REVERSE**_ invert the background and foreground color. 
Usefull to show a selected item in a menu:

```cpp
	wattron(window, A_REVERSE);
	wattroff(window, A_REVERSE);
```

## Troubleshoot

### Latin characters

If ncurses is not printing accented letters try setting locale at the beginning of code

```cpp
	setlocale(LC_ALL, "");
```


[1]: https://linux.die.net/man/3/nonl
