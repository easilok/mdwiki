# Cpp data containers


## Lists

Lists in cpp are linked arrays, meaning they're not stored in continuous memory, each element
points (internal) to the next:

![](uploads/images/linkedlist.png "Linked List")

To use lists it should be included it header, and you can use it several ways:

#### Initializing

```cpp
	#include <list>

	std::list<type> l = { a, b, c};												// Inline initialization
  std::list<type> l;																		// empty list
	std::list<type> l (n, a);															// n elements of <type> with value a
	std::list<type> l (l1.begin(),l1.end());							// iterating through l1 list to initialize
	std::list<type> l (l1);					                      // a copy of l1
```

#### Reading

```cpp
	<type> l.pop_back(); /* Returns and removes the last element of list */
	<type> l.pop_front(); /* Returns and removes the first element of list */
	<type> l.back(): /* Returns last element of list */
	<type> l.front();	/* Returns first element of list */
	bool l.empty(); /* Returns if list if empty */
	unsigned long int l.size(); /* Returns the size of the list */
```


#### Adding

```cpp
	l.push_back(d); /* Adds element d to the end of list */
	l.push_front(e); /* Adds element e to the front of list */
	l.insert (iterator, number_elements, element); /* Insert n times the element in position of iterator */
	/* if ommited number_elements, insert 1 */
	l.assign(number_elements, element); /* Reassign the list with the element given the number_elements times */
```


#### Removing

```cpp
	<type> l.pop_back(); /* Returns and removes the last element of list */
	<type> l.pop_front(); /* Returns and removes the first element of list */
	l.erase(iterator) /* removes selected element */
	l.erase(start_iterator, end_iterator) /* removes range of elements received */
	l.clear(); /* clears all elements of list */
```


#### Navigation

List navigation can only be done by means of iterators, and is described below. Reverse iterators work
in reverse of normal iterators, meaning a _**rbegin**_ iterator poinst to the last element of list,
and its incremention points to previous element on list;

```cpp
	std::list<type>::iterator l.begin();  /* Gets the iterator pointing to the first element of list */
	std::list<type>::iterator l.end();    /* Gets the iterator pointing to the last element of list */
	std::list<type>::iterator l.cbegin(); /* Gets a constant iterator pointing to the first element of list */
	std::list<type>::iterator l.cend();   /* Gets a constant iterator pointing to the last element of list */
	std::list<type>::iterator l.rbegin(); /* Gets a reverse iterator pointing to the last element of list */
	std::list<type>::iterator l.rend();   /* Gets a reverse iterator pointing to the first element of list */

	iterator++, iterator-- /* Poinst iterator to next or previous element in list */

	<type> *iterator /* Gets the value pointed by the iterator */

	std::advance(iterator, n) /* Makes the pointer advance n positions in list */
	std::list<type>::iterator std::next(iterator, n) /* Returns a new iterator pointing to n positions after the iterator given */
	/* the initial iterator keeps its position */
	std::list<type>::iterator std::prev(iterator, n) /* same as before but backwards */
```

#### Operations
```cpp
	l.reverse() /* Reverses the order of the list */
	l.sort() /* Sorts the elements of list in increasing order */
```


## Vector

Vector in cpp is a dynamic array, stored in continuous memory. Compared to lists, insertion in the middle of vector
means all other elements must be reorganized.

![](uploads/images/array.jpg "Vector")

Vector uses the same methods as list for all the operations, adding the capability of acessing an element directly,
but they cannot run methods _**sort**_ or _**reverse**_.

To use vectors it should be included it header, and you can use it several ways:

```cpp
	#include <vector>
	<type> v[n] /* Gets the element in position "n" of the vector "v" */
```
