# Ncurses Learning Sources 


## Documentation

#### Recomended by [Casual Programmer][1]

[tldp - ncurses howto](http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/)

[invisible island - ncurses intro](https://invisible-island.net/ncurses/ncurses-intro.html)


## Tutorials


## Video

[Casual Programmer - Small intro videos channel][1]
[1]: https://www.youtube.com/channel/UCwA85g9HuIgg0SSjX59p0YQ


## Examples

### ncmppcpp

[ncmppcpp - mpd player](https://github.com/arybczak/ncmpcpp)
